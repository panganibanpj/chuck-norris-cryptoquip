/*
	Static strings + numbers, etc.
*/
define([], function()
{
	return {
		//Generic
		NUMBERS: {
			INVALID_INDEX: -1,
			ZERO: 0,
			ONE: 1
		},
		STRINGS: {
			EMPTY: '',
			SPACE: ' '
		},
		TYPES: {
			UNDEFINED: 'undefined'
		}
	};
});
